﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        void PrintCars(BaseCarClass[] carArray)
        {
            foreach (var car in carArray)
            {
                Console.WriteLine(car.ToString());
            }
        }


        void UpdateCars(BaseCarClass[] carArray)
        {
            foreach (var car in carArray)
            {
                Console.WriteLine(car.ToString());
            }


            foreach (var car in carArray)
            {
                car.CalculatemaxMillage(9, 10);

            }


            foreach (var car in carArray)
            {
                AddWeight(10, car);
            }

            foreach (var car in carArray)
            {
                Console.WriteLine(car.ToString());
            }

        }

        void AddWeight(int count, BaseCarClass car)
        {
            if (car is Littletruck)
            {
                Littletruck truck = car as Littletruck;
                truck.bagage += count;
                return;
            }

            if (car is Bus)
            {
                Bus bus = car as Bus;
                bus.people += count;
                return;
            }

            if (car is Truck)
            {
                Truck truck_ = car as Truck;
                truck_.weight += count;
                return;
            }

            if (car is Moto)
            {
                Moto moto = car as Moto;
                moto.tires += count;
                return;
            }

            if (car is Bicycle)
            {
                Bicycle bicycle = car as Bicycle;
                bicycle.frame += count;
                return;
            }
        }

        static void Main(string[] args)
        {
            Littletruck car1 = new Littletruck("liltrack");

            Bus car2 = new Bus("bus  ");

            Truck car3 = new Truck("truck  ");

            Moto car4 = new Moto("moto  ");

            Bicycle car5 = new Bicycle("Bicycle  ");

            BaseCarClass[] cars = new BaseCarClass[] { car1, car2, car3, car4, car5 };

            Program program = new Program();

            program.UpdateCars(cars);

        }
    }



    // weight поле Truck (изначально содержала = 10), при апкасте к базавому типу BaseCarClass, сохраняет в себе класс Truck, но доступа к ней 
    // нету, так как она теперь BaseCarClass (апкаст). И если я её downcast (привeду обратно к типу Truck), то я обратно получу доступ к Truck такой,
    // которой она было до апкаста. Здесь ("void AddWeight (int count, BaseCarClass car)") она неявно апкастнулась. ????
}
