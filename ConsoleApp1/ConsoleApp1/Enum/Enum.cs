﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public enum Enum_Liltruck
    {
        Hatchback,
        sedan,
        coupe,
        leaftback
    }

    public enum Enum_Bus
    {
        Motorcoach,
        Schoolbus,
        Shuttlebus,
        Minibus
    }

    public enum Enum_Truck
    {
        Smalltrucks,
        Lighttrucks,
        Mediumtrucks,
        Heavytrucks
    }

    public enum Enum_Moto
    {
        ScootersandMopeds,
        SportsBikes,
        NakeBikes,
        Cruisers
    }

    public enum Enum_Bicycle
    {
        RoadBike,
        MountainBike,
        TouringBike,
        FoldingBike
    }
}
