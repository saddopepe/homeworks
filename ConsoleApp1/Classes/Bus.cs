﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class Bus : BaseCarClass
    {
        Enum_Bus kar;
        public int people { set; get; }
        public Bus(string name)
        {
            Name = name;
        }

        public Bus(string name, int people, int maxspeed, Enum_Bus type) : this(name)
        {
            kar = type;
            CalculateMillage(people, maxspeed);
        }

        public override string ToString()
        {

            return base.ToString() + "   " + kar + "  " + people;
        }

        public override void CalculatemaxMillage(int a, int b)
        {
            CalculateMillage(a, b);
        }

        void CalculateMillage(int people, int maxspeed)
        {
            Millage = 10 * people * maxspeed;
        }
    }
}
