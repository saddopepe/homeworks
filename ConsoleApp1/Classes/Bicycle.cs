﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class Bicycle : BaseCarClass
    {
        Enum_Bicycle kar;

        public int frame { set; get; }
        public Bicycle(string name)
        {
            Name = name;
        }

        public Bicycle(string name, int weight, int people, Enum_Bicycle type) : this(name)
        {
            kar = type;
            CalculateMillage(people, weight);
        }

        public override string ToString()
        {
            return base.ToString() + " " + kar + "   " + frame;
        }
        public override void CalculatemaxMillage(int a, int b)
        {
            CalculateMillage(a, b);
        }

        void CalculateMillage(int people, int weight)
        {

            Millage = 30 * people * weight;
        }
    }
}
