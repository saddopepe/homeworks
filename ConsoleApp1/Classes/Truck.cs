﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class Truck : BaseCarClass
    {
        Enum_Truck kar;

        public int weight { set; get; }
        public Truck(string name)
        {
            Name = name;
        }

        public Truck(string name, int people, int engine, Enum_Truck type) : this(name)
        {
            kar = type;
            CalculateMillage(people, engine);
        }

        public override string ToString()
        {
            return base.ToString() + " " + kar + "  " + weight;
        }

        public override void CalculatemaxMillage(int a, int b)
        {
            CalculateMillage(a, b);
        }

        void CalculateMillage(int people, int engine)
        {
            if (engine > 1 & engine < 100)
            {
                Millage = 10 * people * engine;
            }
            else
            {
                Console.WriteLine("out of range");
            }
        }
    }
}
