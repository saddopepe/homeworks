﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class Moto : BaseCarClass
    {
        Enum_Moto kar;
        public int tires { set; get; }
        public Moto(string name)
        {
            Name = name;
        }

        public Moto(string name, int people, int metal, Enum_Moto type) : this(name)
        {
            kar = type;
            CalculateMillage(people, metal);
        }

        public override string ToString()
        {
            return base.ToString() + " " + kar + "  " + tires;
        }
        public override void CalculatemaxMillage(int a, int b)
        {
            CalculateMillage(a, b);
        }

        void CalculateMillage(int people, int metal)
        {

            Millage = 20 * people * metal;
        }
    }
}
