﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public abstract class BaseCarClass
    {
        private string name;

        protected string Name
        {
            get
            {
                return name;
            }

            set
            {
                if (value.Length < 10)
                {
                    for (int i = 0; i < value.Length; i++)
                    {
                        if (int.TryParse(Convert.ToString(value[i]), out int a))
                        {
                            Console.WriteLine("не валидная длинна имени");
                            return;
                        }
                    }
                    name = value;
                    Console.WriteLine(" валидная длина имени");
                }
                else
                {
                    Console.WriteLine("ту хьюдж");
                }
            }
        }

        public abstract void CalculatemaxMillage(int a, int b);


        public int Millage;

        public override string ToString()
        {
            Console.WriteLine();
            return Name;
        }
    }
}
