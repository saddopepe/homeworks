﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class Littletruck : BaseCarClass
    {
        Enum_Liltruck kar;

        public int bagage { set; get; }


        public Littletruck(string name)
        {
            Name = name;
        }

        public Littletruck(string name, int people, int maxspeed, Enum_Liltruck type, int bagage) : this(name)
        {
            this.bagage = bagage;
            kar = type;
            CalculateMillage(people, maxspeed);
        }

        public override string ToString()
        {
            return base.ToString() + "  " + kar + "  " + bagage;
        }

        public override void CalculatemaxMillage(int a, int b)
        {
            CalculateMillage(a, b);
        }

        void CalculateMillage(int people, int maxspeed)
        {
            Millage = 10 * people * maxspeed * bagage;
        }
    }
}
